(* :Title: SENotebook *)
(* :Context: SENotebook` *)
(* :Author: R.M *)
(* :Package Version: 0.92b *)
(* :Copyright: R.M, 2012–2013 *)

BeginPackage["SENotebook`"]
Unprotect@"`*";
ClearAll@"`*";

SENotebook::usage = "SENotebook[] opens a new notebook with styles and functionality to convert the notebook to markdown \
for posting on Mathematica StackExchange. SENotebook[\"Examples\"] opens up a notebook with examples of existing \
questions/answers on the site written in this notebook."

(* ::Section:: *)
(* Main functionality *)

Begin["`Private`"]
Needs["SENotebook`StyleSheet`"]
Needs["SENotebook`StackExchangeAPI`"]
Needs["SENotebook`ImageUploader`"]

(* ::Subsection:: *)
(* Conversion to Markdown *)

(* ::Subsubsection:: *)
(* Markdown rules *)
markdownRules["LaTeX"] = {
	HoldPattern[Cell[BoxData[b : FormBox[___, TraditionalForm]], FormatType -> "TraditionalForm"]] :>
		"$" <> ToString[MakeExpression[b, TraditionalForm] /. HoldComplete[x___] :> TeXForm[Unevaluated@x]]<> "$",
	HoldPattern[Cell[BoxData[b : FormBox[___, TraditionalForm]]]] :>
		"$" <> ToString[MakeExpression[b, TraditionalForm] /. HoldComplete[x___] :> TeXForm[Unevaluated@x]]<> "$",
	HoldPattern[StyleBox[s_String, "InlineFormula"]] :>
		"$" <> ToString[MakeExpression[s, TraditionalForm] /. HoldComplete[x___] :> TeXForm[Unevaluated@x]] <> "$",
	HoldPattern[Cell[BoxData[b : FormBox[___, TraditionalForm]], "InlineFormula"]] :>
		"$" <> ToString[MakeExpression[b, TraditionalForm] /. HoldComplete[x___] :> TeXForm[Unevaluated@x]] <> "$",
	HoldPattern[Cell[BoxData[b : FormBox[___, TraditionalForm]], "DisplayFormula"]] :>
		"$$" <> ToString[MakeExpression[b, TraditionalForm] /. HoldComplete[x___] :> TeXForm[Unevaluated@x]] <> "$$\n\n"
}

markdownRules["Styles"] = {
	(* inline code *)
	HoldPattern[StyleBox[s_String, "Input", t___]] /; StringFreeQ[s, "`"] :> StyleBox["`" <> s <> "`", t],
	HoldPattern[StyleBox[s_String, "Input", t___]] :> StyleBox["``" <> s <> " ``", t],

	(* bold/italic/strikethrough text *)
	HoldPattern[StyleBox[s_String, h___, FontSlant -> "Italic", t___]] :> StyleBox["*" <> s <> "*", h, t],
	HoldPattern[StyleBox[s_String, h___, FontWeight -> "Bold", t___]] :> StyleBox["**" <> s <> "**", h, t],
	HoldPattern[StyleBox[s_String, h___, FontVariations -> {___, "StrikeThrough" -> True, ___}, t___]] :> StyleBox["<s>" <> s <> "</s>", h, t],
	HoldPattern[StyleBox[s_String, h___, FontVariations -> {___}, t___]] :> StyleBox[s, h, t] (* ignore all other variations *)
}

markdownRules["Lists"] = {
	(* Unordered lists *)
	HoldPattern[Cell[s_String, "Item"]] :> " - " <> StringReplace[s,
		"\[LineSeparator]\[LineSeparator]" :> "\[LineSeparator]\[LineSeparator] "] <> "\n\n",
	HoldPattern[Cell[TextData[{s___String}], "Item"]] :> " - " <> s <> "\n\n",
	HoldPattern[Cell[s_String, "Subitem"]] :> "  - " <> s <> "\n\n",
	HoldPattern[Cell[TextData[{s___String}], "Subitem"]] :> "  - " <> s <> "\n\n",

	(* Code inside lists *)
	(* Thanks to John Fultz for this answer — http://mathematica.stackexchange.com/a/1411/5 *)
	HoldPattern[c : Cell[_BoxData, "ItemInput"]] :> "\t\t" <>
		StringReplace[First@FrontEndExecute[FrontEnd`ExportPacket[c, "InputText"]], "\n" -> "\n\t"] <> "\n\n",

	(* Quoted text inside lists *)
	HoldPattern[Cell[s_String, "ItemQuote"]] :> " > " <> s <> "\n\n",
	HoldPattern[Cell[TextData[{s___String}], "ItemQuote"]] :> " > " <> s <> "\n\n"
}

markdownRules["Images"] = {
	(* Stand alone images *)
	HoldPattern[c : Cell[BoxData[_GraphicsBox], ___]] :> "![](" <> UploadToSE[c] <> ")\n\n",

	(* Images in input cells *)
	g_GraphicsBox :> RowBox[{"Import", "@","\"\<" <> UploadToSE[Cell@BoxData@g] <> "\>\""}]
}

markdownRules["General"] = {
	(* h1, h2, h3 headings *)
	HoldPattern[Cell[s_String, "Section"]] :> "#" <> s <> "\n\n",
	HoldPattern[Cell[s_String, "Subsection"]] :> "##" <> s <> "\n\n",
	HoldPattern[Cell[s_String, "Subsubsection"]] :> "###" <> s <> "\n\n",
	HoldPattern[StyleBox[s_String, "Text"]] :> s,

	(* regular text *)
	HoldPattern[Cell[TextData[{s___String}], "Text"]] :> s <> "\n\n",
	HoldPattern[Cell[s_String, "Text"]] :> StringJoin[s, "\n\n"],
	HoldPattern[Cell[s_String, TextClipboardType -> "PlainText"]] :> s,

	(* Code block *)
	HoldPattern[c : Cell[_BoxData, "Input"]] :> "\t" <>
		StringReplace[First@FrontEndExecute[FrontEnd`ExportPacket[c, "InputText"]], "\n" -> "\n\t"] <> "\n\n",

	(* Quoted text *)
	HoldPattern[Cell[s_String, "Quote"]] :> "> " <> s <> "\n\n",
	HoldPattern[Cell[TextData[{s___String}], "Quote"]] :> "> " <> s <> "\n\n",

	(* Output & Print *)
	HoldPattern[c : Cell[_BoxData, "Output"]] :> "\t(* " <>
		StringReplace[First@FrontEndExecute[FrontEnd`ExportPacket[c, "InputText"]], "\n" -> "\n\t"] <> " *)\n\n",
	HoldPattern[c : Cell[_BoxData, "Print"]] :> "\t(* " <>
		StringReplace[First@FrontEndExecute[FrontEnd`ExportPacket[c, "InputText"]], "\n" -> "\n\t"] <> " *)\n\n",

	(* Hyperlinks *)
	HoldPattern[ButtonBox[s_String, BaseStyle -> "Hyperlink", ButtonData -> {URL[u_String], ___}, ___]] :> StringJoin["[", s, "](", u, ")"],

	(* Horizontal line *)
	HoldPattern[Cell[_,"HLine"]] :> "---\n\n"
}

markdownRules["Cleanup"] = {
	(* Ignore title styles *)
	HoldPattern[Cell[___,"Title"|"Subtitle"|"Subsubtitle"]] :> Sequence[],

	(* Loose text *)
	HoldPattern[Cell[s_String, ___]] :> s,
	HoldPattern[Cell[TextData[{s__String}], ___]] :> StringJoin[s],
	HoldPattern[Cell[TextData[s_String], ___]] :> s
}

(* ::Subsubsection:: *)
(* Helper functions *)
removeStyleBoxes = HoldPattern[StyleBox[s_String, ___]] :> s
convertToMarkdown[l_] := l //.
	markdownRules["LaTeX"] //.
	markdownRules["Images"] //.
	markdownRules["Styles"] /. removeStyleBoxes //.
	markdownRules["Lists"] //.
	markdownRules["General"] //.
	markdownRules["Cleanup"] // StringJoin

(* ::Subsection:: *)
(* SE notebook *)

(* ::Subsubsection:: *)
(* Code importer *)
validURLQ[url_String] := !StringFreeQ[url, "mathematica.stackexchange.com" | "stackoverflow.com"]

importCode[url_String] /; validURLQ[url] := With[
	{
		filterCode = StringCases[#, ("<pre><code>" ~~ ("\n" ...) ~~ x__ ~~ ("\n" ...) ~~ "</code></pre>") /;
			StringFreeQ[x, "<pre><code>" | "</code></pre>"] :> x] &,
		convertEntities = StringReplace[#, {"&gt;" -> ">", "&lt;" -> "<", "&amp;" -> "&"}] &,
		makeCodeCell = Scan[CellPrint@Cell[Defer@#, "Input", CellTags -> "Ignore"] &, Flatten@{#}] &
	},
	OptionValue["items" /. GetPostInfo@url, "body"] // filterCode // convertEntities // makeCodeCell
]

(* ::Subsubsection:: *)
(* Helper functions *)
copyToClipboard[text_] := Module[{nb},
	nb = NotebookCreate[Visible -> False];
	NotebookWrite[nb, Cell[text, "Text"]];
	SelectionMove[nb, All, Notebook];
	FrontEndTokenExecute[nb, "Copy"];
	NotebookClose[nb];
];

getNotebookData[] := With[
	{
		expr = NotebookGet@EvaluationNotebook[],
		ignoreCells = {HoldPattern[Cell[___, CellTags -> "Ignore"]] -> Sequence[]},
		removeMetaData = {
			HoldPattern[CellChangeTimes -> _] -> Sequence[],
			HoldPattern[CellGroupingRules -> _] -> Sequence[],
			Cell[CellGroupData[c_List, ___]] -> Sequence[c]
		}
	},
	First@DeleteCases[expr //. ignoreCells //. removeMetaData, Except[_Cell | _List]] /. RowBox[a_, b__] :> RowBox[{a, b}]
]

hoverButton[text_String] := Module[
	{
		sText = Style[Text@text, FontSize -> 13, FontFamily -> "Helvetica"],
		bg = RGBColor[251/255, 14/15, 227/255],
		fg = RGBColor[3/5, 47/255, 27/85],
		size, rect
	},
	size = ImageDimensions@Rasterize@sText + {30,20};
	rect = Graphics[
		{
			EdgeForm[None], FaceForm@#, Opacity@0.5, Rectangle[size],
			Inset[Graphics[Style[sText, FontColor -> #2]], Scaled[{0.5, 0.5}]]
		},
		ImageSize -> size, AspectRatio -> (#2/#1 & @@ size)
	] &;

	Mouseover[rect @@ {None, Black}, rect @@ {bg, fg}]
]

getURL[] := DialogInput[
	{str = ""},
	Column[{
		InputField[Dynamic[str], String, FieldHint -> "Enter the URL"],
		DefaultButton@DialogReturn[str]
	}]
]

parentDirectory = $InputFileName ~StringDrop~ -13;
logo = Import[FileNameJoin[{parentDirectory, "Images", "logo.png"}]];

documentation[] := With[{nb = NotebookOpen@FileNameJoin[{parentDirectory, "Documentation.nb"}]},
	NotebookSave@SetOptions[nb,StyleDefinitions -> MathematicaSE];
	SetOptions[nb, Saveable -> False]
]

topBar[s_String] := Cell[
	BoxData@ToBoxes@Switch[s,
		"Full", Graphics[
			{
				First@Show@logo,
				Inset[
					Row[{
						EventHandler[
							hoverButton["Markdown"],
							{"MouseClicked" :> (getNotebookData[] // convertToMarkdown // copyToClipboard)}
						],

						(* Thanks to Rojo for the following workaround using a flag for EventHandler *)
						DynamicModule[{flag = 0},
							DynamicWrapper[
								EventHandler[
									hoverButton["Import Code"],
									{"MouseClicked" :> ++flag}
								],
								Refresh[
									If[
										flag > 0,
										importCode@DialogInput[{url = ""},
											Column[{
												InputField[Dynamic[url], String, FieldHint -> "Enter the URL"],
												DefaultButton@DialogReturn[url]
											}]
										]
									],
									TrackedSymbols :> {flag}
								],
								SynchronousUpdating -> False
							]
						],

						EventHandler[
							hoverButton["Documentation"],
							{"MouseClicked" :> documentation[]}
						]
					}],
					{520, 15}
				]
			},
			ImageSize -> ImageDimensions@logo, ImagePadding -> 0, PlotRangePadding -> 0
		],
		"Plain", Image[logo, Magnification->1]
	],

	Background -> White,
	CellFrame -> {{0, 0}, {2, 0}},
	CellFrameMargins -> 0,
	CellFrameColor -> GrayLevel[0.8],
	CellMargins -> {{0, 0}, {10, 0}},
	CellTags -> "Ignore"
]

(* ::Subsubsection:: *)
(* Create notebook *)

SENotebook[] := With[{nb = CreateDocument[]},
	SetOptions[nb,
		DockedCells -> topBar["Full"],
		ShowCellLabel -> False,
		Saveable -> True,
		StyleDefinitions -> MathematicaSE,
		CellBracketOptions -> {"Color" -> GrayLevel[0.9]},
		DefaultNewCellStyle -> "Text",
		(* Thanks to Rojo for getting NotebookEventActions to work in a horizontal insertion point *)
		NotebookEventActions -> {{"KeyDown","\t"} :>
			If[
				NotebookTools`HorizontalInsertionPointQ[EvaluationNotebook[]],
				(NotebookWrite[EvaluationNotebook[],Cell[BoxData[""],"Input"]]; SelectionMove[EvaluationNotebook[], Previous, CellContents])
			]
		},
		TrackCellChangeTimes -> False
	]
]

SENotebook["Examples"] := With[{nb = NotebookOpen@FileNameJoin[{parentDirectory, "Examples.nb"}]},
	NotebookSave@SetOptions[nb,StyleDefinitions -> MathematicaSE];
	SetOptions[nb, Saveable -> False]
]
End[]

SENotebook[]
EndPackage[]
