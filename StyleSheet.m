(* :Title: StyleSheet *)
(* :Context: SENotebook`StyleSheet` *)
(* :Author: R.M *)
(* :Package Version: 0.91b *)
(* :Copyright: R.M, 2012–2013 *)

BeginPackage["SENotebook`StyleSheet`"]

Clear@MathematicaSE;

Begin["`Private`"]
cellMargin = {{50, 50}, {10, 0}};
magenta = RGBColor[3/5, 47/255, 27/85];

MathematicaSE := Notebook[
	{
		Cell[StyleData[StyleDefinitions -> "Default.nb"]],

		Cell[
			StyleData["StandardForm"],
			AutoStyleOptions->{
				"CommentStyle"->{
					FontColor -> RGBColor[1/3, 1/3, 1/3],
					ShowAutoStyles -> False,
					ShowSyntaxStyles -> False,
					AutoNumberFormatting -> False
				},
				"FunctionLocalVariableStyle"->{FontColor -> RGBColor[12/17, 8/17, 0]},
				"LocalScopeConflictStyle"->{FontColor -> RGBColor[0.423529, 0.443137, 0.768627]},
				"LocalVariableStyle"->{FontColor -> RGBColor[0.164706, 0.631373, 0.596078]},
				"PatternVariableStyle"->{FontColor -> RGBColor[0, 2/5, 0], FontSlant -> Italic},
				"StringStyle"->{
					FontColor -> RGBColor[139/255, 1/5, 1/5],
					ShowAutoStyles -> False,
					ShowSyntaxStyles -> False,
					AutoNumberFormatting -> False
				},
				"SymbolShadowingStyle"->{FontColor -> RGBColor[0.796078, 0.294118, 0.0862745]},
				"SyntaxErrorStyle"->{FontColor -> RGBColor[0.862745, 0.196078, 0.184314]},
				"UndefinedSymbolStyle"->{FontColor -> RGBColor[10/51, 106/255, 179/255]}
			},
			FontFamily->"Menlo",
			FontSize->13,
			FontWeight->"Plain",
			FontSlant->"Plain"
		],

		Cell[
			StyleData["Input"],
			StyleKeyMapping -> {"Backspace" -> "Text"},
			CellMargins->cellMargin,
			AutoIndent->True,
			AutoSpacing->True,
			FontFamily->"Menlo",
			FontWeight->"Plain",
			FontColor->GrayLevel[0],
			FontSize -> 13,
			Background->RGBColor[81/85, 81/85, 81/85]
		],

		Cell[
			StyleData["Output"],
			FontColor -> RGBColor[1/3, 1/3, 1/3]
		],

		Cell[
			StyleData["Text"],
			StyleKeyMapping -> {"Tab" -> "Input", ">" -> "Quote"},
			CellMargins->cellMargin,
			FontFamily -> "Arial",
			FontSize -> 13
		],

		Cell[
			StyleData["Quote"],
			StyleKeyMapping -> {"Backspace" -> "Text"},
			CellFrame->{{1, 0}, {0, 0}},
			CellMargins->cellMargin,
			CellFrameColor->RGBColor[50/51, 42/85, 1/15],
			Background->RGBColor[84/85, 83/85, 14/15],
			FontFamily -> "Arial",
			FontSize -> 13,
			MenuCommandKey->">",
			MenuSortingValue -> 6
		],

		Cell[
			StyleData["Section"],
			CellFrame->0,
			CellMargins->cellMargin,
			FontFamily->"Arial",
			FontSize->22,
			FontWeight -> Bold,
			FontColor->GrayLevel[0],
			MenuSortingValue -> 1
		],

		Cell[
			StyleData["Subsection"],
			CellFrame->0,
			CellMargins->cellMargin,
			FontFamily->"Arial",
			FontSize->18,
			FontWeight -> Bold,
			FontColor->GrayLevel[0],
			MenuSortingValue -> 2
		],

		Cell[
			StyleData["Subsubsection"],
			CellFrame->0,
			CellMargins->cellMargin,
			FontFamily->"Arial",
			FontSize->15,
			FontWeight -> Bold,
			FontColor->GrayLevel[0],
			MenuSortingValue -> 3
		],

		Cell[
			StyleData["Item"],
			CellDingbat->StyleBox["\[FilledSmallCircle]", Alignment -> Baseline, RGBColor[0,0,0]],
			StyleKeyMapping -> {
				"Tab" -> "Subitem",
				">" -> "ItemQuote",
				KeyEvent["Return", Modifiers -> {Shift}] -> "ItemParagraph",
				"Backspace" -> "Text"
			},
			CellFrame->0,
			CellMargins->{{75, 50}, {10, 0}},
			FontFamily->"Arial",
			FontSize->13,
			ShowCellLabel -> True,
			FontColor->GrayLevel[0],
			MenuCommandKey->"*",
			MenuSortingValue -> 4
		],

		Cell[
			StyleData["Subitem"],
			CellDingbat->StyleBox["\[FilledSmallSquare]", Alignment -> Baseline, GrayLevel[0]],
			StyleKeyMapping -> {"Tab" -> "ItemInput", "Backspace" -> "Item"},
			CellFrame->0,
			CellMargins->{{100, 50}, {10, 0}},
			FontFamily->"Arial",
			FontSize->13,
			ShowCellLabel -> True,
			FontColor->GrayLevel[0],
			MenuSortingValue -> 5
		],

		Cell[
			StyleData["ItemInput", StyleDefinitions -> StyleData["Input"]],
			StyleKeyMapping -> {"Backspace" -> "Item"},
			CellMargins->{{75, 50}, {10, 0}},
			AutoIndent->True,
			AutoSpacing->True,
			FontWeight->"Plain",
			FontColor->GrayLevel[0],
			FontSize -> 13,
			Background->RGBColor[81/85, 81/85, 81/85]
		],

		Cell[
			StyleData["ItemQuote", StyleDefinitions -> StyleData["Quote"]],
			StyleKeyMapping -> {"Backspace" -> "Item"},
			CellMargins->{{75, 50}, {10, 0}}
		],

		Cell[
			StyleData["InlineFormula"],
			DefaultFormatType -> "TraditionalForm",
			FontFamily -> "Palatino",
			FontSize -> 13,
			StyleMenuListing -> False
		],

		Cell[
			StyleData["DisplayFormula"],
			DefaultFormatType -> "TraditionalForm",
			FontFamily -> "Palatino",
			FontSize -> 15,
			TextAlignment -> Center,
			MenuCommandKey -> "@",
			MenuSortingValue -> 8
		],

		Cell[
			StyleData["HLine"],
			Editable->False,
			Selectable->False,
			CellFrame->{{0, 0}, {0, 1}},
			ShowCellBracket->False,
			CellMargins-> cellMargin,
			CellElementSpacings->{"CellMinHeight"->0},
			CellFrameMargins->0,
			CellFrameColor->GrayLevel[0.5]
		],

		Cell[StyleData["Hyperlink"],
 			FontColor-> magenta
		],

		Cell[StyleData["Title"],
			CellMargins -> cellMargin,
			FontColor -> magenta,
			FontSize -> 28
		],

		Cell[StyleData["Subtitle"],
			CellMargins -> cellMargin,
			FontColor -> magenta,
			FontSize -> 20
		],

		Cell[StyleData["Subsubtitle"],
			CellMargins -> cellMargin,
			FontColor -> magenta,
			FontSize -> 16
		]
	},
	Saveable -> False,
	StyleDefinitions -> "PrivateStylesheetFormatting.nb"
]

End[]

EndPackage[]