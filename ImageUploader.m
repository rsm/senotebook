(* :Title: ImageUploader *)
(* :Context: SENotebook`ImageUploader` *)
(* :Author: Sz. Horvát *)
(* :Modified by: R.M, Patrick*)
(* :Copyright: Sz. Horvát *)

BeginPackage["SENotebook`ImageUploader`"];
Clear@UploadToSE;

Begin["`Private`"]
Needs["JLink`"];
JLink`InstallJava[];

UploadToSE::httperr = "Server returned respose code: `1`";
UploadToSE::err = "Server returner error: `1`";

UploadToSE[img_] := Module[
	{getVal, url, client, method, data, partSource, part, entity, code, response, error, result},
	getVal[res_, key_String] := With[{k = "var " <> key <> " = "},
		StringTrim[
			First@StringCases[
				First@Select[res, StringMatchQ[#, k ~~ ___] &],
				k ~~ v___ ~~ ";" :> v
			],
			"'"
		]
	];

	data = ExportString[rasterize@img, "PNG"];

	JLink`JavaBlock[
		JLink`LoadJavaClass["de.halirutan.uploader.SEUploader", StaticsVisible->True];
		response = Check[SEUploader`sendImage[ToCharacterCode[data]], Return[$Failed]]
	];

	If[response === $Failed, Return[$Failed]];
	response = StringTrim /@ StringSplit[response, "\n"];
	error = getVal[response, "error"];
	result = getVal[response, "result"];
	If[StringMatchQ[result, "http*"], result, Message[UploadToSE::err, error]; $Failed]
]

rasterize[img_, maxWidth_: 650] := Module[{nb, image},
	nb = CreateDocument[{}, WindowSelected -> False, Visible -> False, WindowSize -> maxWidth];
	NotebookWrite[nb, img];
	image = Rasterize[nb, "Image"];
	NotebookClose[nb];
	image
];

End[];
EndPackage[]
