(* :Title: StackExchangeAPI *)
(* :Context: SENotebook`StackExchangeAPI` *)
(* :Author: R.M *)
(* :Package Version: 0.1 *)
(* :Copyright: R.M, 2012–2013 *)

BeginPackage["SENotebook`StackExchangeAPI`"]
GetPostInfo::usage = ""

Begin["`Private`"]
	(* filters *)
	filter["getBody"] := "!9hnGsretg"

	parsePostURL[url_String] := {First@StringCases[#, Shortest[s__] ~~ "." ~~ ___ :> s], #2, #3} & @@
		StringSplit[StringDrop[url, 7], "/"][[;;3]]

	GetPostInfo[url_String] := Import[ToString@
		StringForm["http://api.stackexchange.com/2.1/posts/`1`?site=`2`&filter=`3`", #3, #1, filter["getBody"]] & @@ parsePostURL[url],
		"JSON"];

End[];

EndPackage[]